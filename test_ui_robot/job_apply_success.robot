***Settings***
Library    SeleniumLibrary

***Variables***


***Keywords***
คุณรัชชานนท์หางาน Programmer และสมัครงานสำเร็จ
  #Q: ต้องทำอะไรบ้าง
    เข้าเว็ปไปที่หน้าค้นหา
    พิมพ์คำว่า "Programmer" และกดปุ่มคนหา
    หน้าเว็บจะขึ้นอาชีพ Programmer 4 ตัว
    เลือกตำแหน่งงาน Python Programmer
    กดสมัคร
    ระบบแสดงผลลัพธ์ว่า สมัครงานสำเร็จ

***Test Cases***
เข้าเว็ปไปที่หน้าค้นหา
    Open Browser    http://localhost:3000/searchJob    chrome
    Set Selenium Speed     0.5

พิมพ์คำว่า "Programmer" และกดปุ่มคนหา
    Input Text    id=search_text    Programmer
    Click Element    id=search_button

หน้าเว็บจะขึ้นอาชีพ Programmer 4 ตัว
    Element Text Should Be    id=name_1    Python Programmer

เลือกตำแหน่งงาน Python Programmer
    Click Element    id=show_detail_1
    Wait Util Element Contains    id=name    Python Programmer

กดสมัคร
    Click Element    id=apply_job

ระบบแสดงผลลัพธ์ว่า สมัครงานสำเร็จ
    Element Should Contains    id=message    Apply_job
    